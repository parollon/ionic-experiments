import { Component, OnInit } from '@angular/core';
import { App } from '@capacitor/app';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-version',
  templateUrl: './version.component.html',
  styleUrls: ['./version.component.scss'],
})
export class VersionComponent implements OnInit {

  version:string= "NO_VERSION"
  constructor( private platform: Platform) { 

    if (this.platform.is('capacitor')) {
      App.getInfo().then(data=>{
        this.version = data['version']})
    }else{
      console.log("It works, but you are not in capacitor/android/ios");
    }
  }
  ngOnInit() {}

}

