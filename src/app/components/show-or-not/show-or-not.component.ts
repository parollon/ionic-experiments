import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'show-or-not',
  templateUrl: './show-or-not.component.html',
  styleUrls: ['./show-or-not.component.scss'],
})
export class ShowOrNotComponent implements AfterViewInit,OnChanges {
  @Input() show: boolean
  
  @Input() msgShow:string="You can see"

  backup:string
  constructor() {
    this.backup=this.msgShow
   }

  ngAfterViewInit(){
    if(this.show===false){
      this.msgShow=""
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log("Component chamnge detected from ",changes.show.previousValue,"=>",changes.show.currentValue);
    this.show= changes.show.currentValue
    if(changes.show.currentValue===false){
      this.msgShow=""
    }else{
      this.msgShow=this.backup
    }
  }

}
