import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'live-time',
  templateUrl: './livetime.component.html',
  styleUrls: ['./livetime.component.scss'],
})
export class LiveTimeComponent implements OnInit {
  timeString:string =  new Date().toString()
  constructor() {     
    setInterval(()=>{
      let current = new Date()
      this.timeString = current.toString() 
    },1000)
  }

  ngOnInit() {}

}
