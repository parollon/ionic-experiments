// videojs.ts component
//video example :https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4

import { Component, ElementRef, Input, OnDestroy, OnChanges, ViewChild, AfterViewInit, OnInit, SimpleChanges } from '@angular/core';
/*import videojs from 'video.js';*/
@Component({
  selector: 'app-videojs',
  templateUrl: './vj-player.component.html',
  styleUrls: ['./vj-player.component.scss'],
})
export class VjPlayerComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  //@ViewChild('target', { static: true }) target: ElementRef;
  @ViewChild('target', { static: true }) target: ElementRef;
  // see options: https://github.com/videojs/video.js/blob/maintutorial-options.html
  @Input() options: {
    fluid: boolean,
    aspectRatio: string,
    autoplay: boolean,
    sources: {
      src: string,
      type: string,
    }[],
  };

  @Input() dynamicOpts: any = "NODYNAMIC"





 // player: videojs.Player;

  constructor( private elementRef: ElementRef ) { }

  ngAfterViewInit() {
  }

  ngOnInit() {
    
    /*this.player = videojs(this.target.nativeElement,
      this.options, function onPlayerReady() {
        console.log('onPlayerReady', this);
      });*/
  }

  ngOnDestroy() {
    // destroy player
   /* if (this.player) {
      this.player.dispose();
    }*/
  }
  ngOnChanges(changes: SimpleChanges): void {
  /* this.dynamicOpts = changes?.dynamicOpts?.currentValue
    if(changes.dynamicOpts && this.dynamicOpts){
      console.log("Previous:", changes.dynamicOpts.previousValue);
      console.log("Currents:", changes.dynamicOpts.currentValue);

     // if(this.player){
     //   this.player.src({ type: 'video/mp4', src: changes.dynamicOpts.currentValue });

      //}*/
      /*
      console.log(this.options.sources);
      this.options.sources =[{ src: changes.dynamicOpts.currentValue , type: 'video/mp4' }]

      /
      options: { fluid: boolean, aspectRatio: string, autoplay: boolean, sources: { src: string,type: string,}[],};
          
      
      { autoplay:false, controls:true, sources: [{ src: 'https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4', type: 'video/mp4' }]}
      /


      this.player = videojs(this.target.nativeElement,
        this.options, function onPlayerReady() {
          console.log('onPlayerReady', this);
        });*/
    /*}else{
      console.log("No url changes");
    }*/


  }

}

/*

To create a DOM element, you use the createElement() method.

    const element = document.createElement(htmlTag); ...
    const e = document.createElement('div'); ...
    e.innerHTML = 'JavaScript DOM'; ... 

const btn = document.createElement("p");
btn.innerHTML = "Hello Button";
document.body.appendChild(btn);

 */