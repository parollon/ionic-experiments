import { Injectable } from '@angular/core';
import { Filesystem,Directory } from '@capacitor/filesystem';
import { CapacitorStorageService } from './capacitor-storage.service';
@Injectable({
  providedIn: 'root'
})
export class VideoService {
  public videos =[]
  private VIDEOS_KEY; string ='videos';

  constructor(public storage:CapacitorStorageService) { }

  async loadVideos(){
    const videoList = await this.storage.get<[]>(this.VIDEOS_KEY)
    this.videos = (videoList!=undefined) ? videoList :[]
    return this.videos
  }
  async storeVideo(blob){
    const fileName = new Date().getTime()+".mp4"
    const base64Data = await this.convertBlobToBase64(blob) as string;
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data
    });
 
    // Add file to local array
    this.videos.unshift(savedFile.uri);
 
    // Write information to storage
    return this.storage.set(this.VIDEOS_KEY,this.videos)
  }
  // Helper function
  private convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
      const reader = new FileReader;
      reader.onerror = reject;
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.readAsDataURL(blob);
    });
   
    // Load video as base64 from url
    async getVideoUrl(fullPath) {
      const path = fullPath.substr(fullPath.lastIndexOf('/') + 1);
      const file = await Filesystem.readFile({
        path: path,
        directory: Directory.Data
      });
      return `data:video/mp4;base64,${file.data}`;
    }
  
}
