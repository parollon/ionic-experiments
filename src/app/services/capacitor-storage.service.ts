import { Injectable } from '@angular/core';
import { ConfigureOptions, GetResult, KeysResult, MigrateResult, RemoveOptions, Storage } from '@capacitor/storage';
@Injectable({
  providedIn: 'root'
})
export class CapacitorStorageService {

  constructor() { }

  public async set(_key: string, _value: any):Promise<void> {
    await Storage.set({ key: _key, value: JSON.stringify(_value) });
  }
  public async get<T>(_key:string): Promise<T>{
    let a: GetResult  =  await Storage.get({ key: _key});    
    if (a !=null && a!=undefined && a.value !=null && a.value!=undefined ){
      return JSON.parse(a.value)
    }
    return  undefined;
  }

  public async remove(_key:string):Promise<void>{
    await Storage.remove({ key: _key });
  }

  public async clear():Promise<void>{
    await Storage.clear()
  }

  public async keys():Promise<String[]>{
  
    let  keySet:KeysResult = await Storage.keys()
    if (keySet !=null && keySet!=undefined && keySet.keys !=null && keySet.keys!=undefined ){
      return keySet.keys
    }
    return []
  }

  public async configure(_options:ConfigureOptions):Promise<void>{
    await Storage.configure(_options)
  }
  public async migrate(): Promise<MigrateResult>{
    return await Storage.migrate()
  }
  public async removeOld(_options:RemoveOptions): Promise<void>{
    await Storage.remove(_options)
  }
}
