import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-components',
  templateUrl: './components.page.html',
  styleUrls: ['./components.page.scss'],
})
export class ComponentsPage implements OnInit {

  harcodedFalse=false
  harcodedTrue=true
  dynamicBoolean=false
  constructor() { }

  ngOnInit() {
  }
  change_boolean(){
    if(!this.dynamicBoolean){
      this.dynamicBoolean=true
    }else{
      this.dynamicBoolean =false
    }
    console.log(this.dynamicBoolean);
    
  }

}
