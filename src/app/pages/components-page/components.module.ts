import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ComponentsPageRoutingModule } from './components-routing.module';
import { ComponentsPage } from './components.page';
import { CustomComponentsModule } from 'src/app/components/custom-components.module';


@NgModule({
  imports: [
    //components
    CustomComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsPageRoutingModule
  ],
  declarations: [ComponentsPage]
})
export class ComponentsPageModule {}
