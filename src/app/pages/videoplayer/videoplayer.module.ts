import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VideoplayerPageRoutingModule } from './videoplayer-routing.module';

import { VideoplayerPage } from './videoplayer.page';

import { CustomComponentsModule } from 'src/app/components/custom-components.module';


@NgModule({
  imports: [    //components
  CustomComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    VideoplayerPageRoutingModule
  ],
  declarations: [VideoplayerPage]
})
export class VideoplayerPageModule {}
