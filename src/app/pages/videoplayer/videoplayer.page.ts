/*import { Component,OnInit} from '@angular/core';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
*/



import { Component,OnInit } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { ModalController } from '@ionic/angular';
import { FullscreenPage } from 'src/app/modals/fullscreen/fullscreen.page';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { Http } from '@capacitor-community/http';
import { HttpDownloadFileResult } from '@capacitor-community/http';


const VIDEO_DIR = '';
interface LocalFile {
  name: string;
  path: string;
  data: string;
}

//http://jsfiddle.net/dsbonev/cCCZ2/
@Component({
  selector: 'app-videoplayer',
  templateUrl: './videoplayer.page.html',
  styleUrls: ['./videoplayer.page.scss'],
})

export class VideoplayerPage implements OnInit {

  donwloadUrl='';
  //////////////////////////
  videos: LocalFile[] = []; //Load files
  local_videos:String[] =[]
  previous_URL=""
  video_URL=""

  files:File[]
  constructor(public modalCtrl: ModalController) {}
  async ngOnInit() {
    await Filesystem.checkPermissions().then(async result=>{})
    await Filesystem.requestPermissions()
    await this.loadFiles()
  }
  async loadFiles() {
    console.log("loadFiles")
    this.videos =[]

    //LOADER START
    Filesystem.readdir({
      path: VIDEO_DIR,
      directory:Directory.ExternalStorage,
    }).then(result => {
      console.log("Folder exists!")
      console.log("loadFiles:Then",result.files)
      this.loadFileData(result.files);
    }).catch(async err=>{
      console.log("error",err);
      // Folder does not yet exists!
      console.log("Folder does not yet exists!")
      await Filesystem.mkdir({
        path: VIDEO_DIR,
        directory: Directory.ExternalStorage,
      }).then((data)=>{console.log("Folder created")});
    })
  }

  async getURLvideo(url_local?){
    console.log(url_local);
    console.log("input:", this.video_URL)
    
    if (this.video_URL){
      console.log("input:", this.video_URL)
      //this.previous_URL=this.video_URL
      const modal = await this.modalCtrl.create({
        component: FullscreenPage,
        componentProps: {
          url: this.video_URL,      
          platform: Capacitor.getPlatform()
        },
        swipeToClose: true
      });
      await modal.present();
      return
    }
    console.log("No URL changed")
    
  }

  async loadFileData(fileNames: string[]) {
    for (let f of fileNames) {

      const uri = await Filesystem.getUri({
        path: VIDEO_DIR,
        directory:Directory.ExternalStorage,
      })
      
      const filePath = `${uri.uri}/${f}`;
      this.local_videos.push(filePath)
      
      
      console.log(filePath);
      
      
    }
  }
  //////////////////
  async downloadVideo(url?) {

    this.donwloadUrl = url ? url : this.donwloadUrl;
    if(this.donwloadUrl){

      let documentDownloadedName= this.donwloadUrl.substring(this.donwloadUrl.lastIndexOf('/')+1)
      console.log(documentDownloadedName);
      
      const options = {
        url: this.donwloadUrl,
        filePath:`${VIDEO_DIR}/${documentDownloadedName}`,
        fileDirectory: Directory.ExternalStorage, ///[storage/emulated/0/]/whatever.pdf
        // Optional
        method: 'GET',
      };
    
      // Writes to local filesystem
      const response: HttpDownloadFileResult = await Http.downloadFile(options);
      console.log(response);
    
      const uriPath = await Filesystem.getUri({
        directory: Directory.ExternalStorage,
        path: documentDownloadedName
      });
      console.log(uriPath.uri);
      const modal = await this.modalCtrl.create({
        component: FullscreenPage,
        componentProps: {
          url: uriPath.uri,      
          platform: Capacitor.getPlatform()
        },
        swipeToClose: true
      });
      await modal.present();

    


      
      

    }
  }

}
