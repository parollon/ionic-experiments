import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FullScreenBackgroundPage } from './full-screen-background.page';

const routes: Routes = [
  {
    path: '',
    component: FullScreenBackgroundPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullScreenBackgroundPageRoutingModule {}
